#include "PriorityQueue.h"
#include <iostream>

PriorityQueue::~PriorityQueue() {}


PriorityQueue::PriorityQueue(const int n, function<int(int)>priorityFunction) : priorityFunction(priorityFunction) {
    keyToIndex.resize(n);
    heap.resize(n);
}

int PriorityQueue::extractKeyMinimum() {
    int keyMin = heap[0];
    heap[0] = heap[heapSize-1];
    heapSize--;
    down(0);
    return keyMin;
}

int PriorityQueue::updateDecreaseKey(const int key) {up(keyToIndex[key]);}

int PriorityQueue::getIndexFather(const int i) const {if(i == 0) return 0; else return (i-1)/2;}
int PriorityQueue::getIndexLeftChild(const int i) const {if(2*i+1 < heapSize) return 2*i+1; else return i;}
int PriorityQueue::getIndexRightChild(const int i) const {if(2*i+2 < heapSize) return 2*i+2; else return i;}


void PriorityQueue::up(const int i) {
    const int ifather = getIndexFather(i);
    if(priorityFunction(heap[i]) < priorityFunction(heap[ifather])) {
        swap(heap[i], heap[ifather]);
        keyToIndex[heap[i]] = i;
        keyToIndex[heap[ifather]] = ifather;
        up(ifather);
    }
}

int PriorityQueue::getIndexLocalMin(const int i) const {
    const int j = getIndexLeftChild(i);
    const int k = getIndexRightChild(i);

    const int pi = priorityFunction(heap[i]);
    const int pj = priorityFunction(heap[j]);
    const int pk = priorityFunction(heap[k]);

    int pmin = pi;
    if(pj < pmin) pmin = pj;
    if(pk < pmin) pmin = pk;

    if(pmin == pi) return i;
    if(pmin == pj) return j;
    if(pmin == pk) return k;
}


const bool PriorityQueue::isEmpty() const {return (heapSize == 0);}

void PriorityQueue::down(const int i) {
    const int j = getIndexLocalMin(i);
    if(j != i) {
        swap(heap[i], heap[j]);
        swap(keyToIndex[heap[i]], keyToIndex[heap[j]]);
        down(j);
    }
}

const void PriorityQueue::print() {
    for(int i = 0; i < heapSize; i++)
        cout << heap[i] << "  ";
    cout << "\n";
}

void PriorityQueue::insert(const int key) {
    const int i = heapSize;
    heap[i] = key;
    keyToIndex[key] = i;
    heapSize++;
    up(i);
}




void testPriorityQueue() {
    PriorityQueue Q(20, [] (int i) {return i;});

    Q.insert(2);
    Q.print();
    Q.insert(5);
    Q.print();
    Q.insert(1);
    Q.print();
    Q.insert(8);
    Q.print();
    Q.insert(12);
    Q.print();
    Q.insert(15);
    Q.print();
    Q.insert(0);
    Q.print();

    Q.print();
    while(!Q.isEmpty())
        cout << Q.extractKeyMinimum() << "\n";
}
