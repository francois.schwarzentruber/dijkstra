#include "Graph.h"

Graph::Graph(const int n) {
    edges.resize(n);
}

Graph::~Graph()
{
    //dtor
}


void Graph::addEdge(const int source, const int destination, const int weight) {
    edges[source].push_front({destination, weight});
}


const int Graph::getNumberOfNodes() {
    return edges.size();
}


const list<Edge> Graph::getEdges(const int source) const {return edges[source];}
