#ifndef PRIORITYQUEUE_H
#define PRIORITYQUEUE_H

#include <functional>
#include <vector>

using namespace std;


class PriorityQueue {
    public:

        /*
        @param n
        @param priorityFunction
        Creates a priority queue. Keys are supposed to be in the interval [0, ..., n-1]
        The function priorityFunction given as a parameter is supposed to compute the
        priority for a given key. It is a min-heap.
        */
        PriorityQueue(const int n, std::function<int(int)> priorityFunction);
        virtual ~PriorityQueue();

        /*
        Removes the most prioritized key (its priority is minimum)
        @return that key
        */
        int extractKeyMinimum();

        /*
        @param key
        Update the heap according to the new priority of key (whose value has decreased)
        */
        int updateDecreaseKey(const int key);

        /*
        @param key
        add a new key
        */
        void insert(const int key);

        /*
        @return true iff the queue is empty*/
        const bool isEmpty() const;

        /*
        print the content of the heap (for debug)
        */

        const void print();
    protected:

    private:
        function<int(int)> priorityFunction; //we store the priority function
        vector<int> keyToIndex; //a dictionnary key => the index in the heap
        vector<int> heap; // the tree of the heap stored in an array. We jave heap[keyToIndex[k] = k
        int heapSize = 0; //number of keys in the heap
        void up(const int i); //@param i is the index in the array heap
        void down(const int i);
        int getIndexLocalMin(const int i) const; //computes the index in the heap of the most prioritized key among i and its children in the tree

        /*
        for computing the indices of the father and the children. If no father (because i = 0, i.e. the root) or no such child, all these functions return i itself.
        */
        int getIndexFather(const int i) const;
        int getIndexLeftChild(const int i) const;
        int getIndexRightChild(const int i) const;


};

/*
function that tests the priority queue class
*/
void testPriorityQueue();


#endif // PRIORITYQUEUE_H
