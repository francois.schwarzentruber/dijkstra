#ifndef GRAPH_H
#define GRAPH_H

#include <vector>
#include <list>

using namespace std;

/*
stores an edge (the source is implicit)
*/
struct Edge {
    int destination;
    int weight;
};


class Graph {
    public:
    /*
    Creates a graph with n nodes: 0, 1, ... n-1. The graph does not contain any edge.
    */
        Graph(const int n);
        virtual ~Graph();

        /*
        add an edge source ---> destanation with a given weight
        */
        void addEdge(const int source, const int destination, const int weight);

        /*
        @returns the number of nodes
        */
        const int getNumberOfNodes();

        /*
        @param source a node
        @returns the list of edges from the source
        */
        const list<Edge> getEdges(const int source) const;
    protected:

    private:
        vector<list<Edge>> edges; //stores the adjacency lists. One for each node.
};

#endif // GRAPH_H
