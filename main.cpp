#include <iostream>
#include "PriorityQueue.h"
#include "Graph.h"

using namespace std;

#define INFINITY 100000

/*
Dijkstra's alorithm
@param a graph G
@param a source node
@returns an array of the distances
*/
vector<int> dijkstra(Graph G, int source) {
    vector<int> d(G.getNumberOfNodes(), +INFINITY);
    d[source] = 0;

    PriorityQueue Q(G.getNumberOfNodes(), [&d](int u) {return d[u];});
    for(int u = 0; u < G.getNumberOfNodes(); u++)
        Q.insert(u);

    while(!Q.isEmpty()) {
        int u = Q.extractKeyMinimum();
        for(auto edge : G.getEdges(u))
            if(d[edge.destination] > d[u] + edge.weight) {
                    d[edge.destination] = d[u] + edge.weight;
                    Q.updateDecreaseKey(edge.destination);
            }
    }
    return d;
}


int main() {
    Graph G(5);

    #define A 0
    #define B 1
    #define C 2
    #define D 3
    #define E 4

    G.addEdge(A, B, 4);
    G.addEdge(A, C, 2);
    G.addEdge(B, C, 3);
    G.addEdge(B, D, 2);
    G.addEdge(B, E, 3);
    G.addEdge(C, B, 1);
    G.addEdge(C, D, 4);
    G.addEdge(E, D, 1);

    vector<int> d = dijkstra(G, 0);

    for(int node = 0; node < G.getNumberOfNodes(); node++)
        cout << "node " << node << ": " << " at distance " << d[node] << " from node 0\n";
}
